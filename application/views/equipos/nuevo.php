<!-- nuevo.php -->
<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVO EQUIPO
  </b>
</h1>
<br>

<form class="" action="<?php echo site_url('equipos/guardarEquipo');?>" method="post" enctype="multipart/form-data" id="formulario_equipo">
  <div class="row">
    <div class="col-md-6">
      <br>
      <label for="">NOMBRE:</label>
      <input type="text" name="nombre_equi" id="nombre_equi" class="form-control" placeholder="Ingrese el nombre">
      <br>

      <label for="">SIGLAS:</label>
      <input type="text" name="siglas_equi" id="siglas_equi" class="form-control" placeholder="Ingrese las siglas">
      <br>

      <label for="">FUNDACIÓN:</label>
      <input type="number" name="fundacion_equi" id="fundacion_equi" class="form-control" placeholder="Ingrese el año de fundación">
      <br>

      <label for="">REGIÓN:</label>
      <input type="text" name="region_equi" id="region_equi" class="form-control" placeholder="Ingrese la región">
      <br>

      <label for="">NÚMERO DE TÍTULOS:</label>
      <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" class="form-control" placeholder="Ingrese el número de títulos">
      <br>
    </div>
    <div class="col-md-6">
      <br>
      <div class="row">
        <div class="col-md-12">
          <br>
          <img src="https://mifutbolecuador.futbol/wp-content/uploads/2023/02/liga-pro-2023-equipos.png?w=1000" style="width: 600px;" alt="">
          <br>
          <br>
          <br>
          <div class="col-md-12 text-center">

            <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save"></i> GUARDAR</button>
            <a href="<?php echo site_url('equipos/index');?>" class="btn btn-danger"> <i class="fa fa-ban"></i>
              CANCELAR
            </a>
          </div>

        </div>
      </div>


  </div>
</form>
<br><br>


<script type="text/javascript">
    $("#formulario_equipo").validate({
        rules:{
            "nombre_equi": {
                required: true,
                minlength: 2,
                maxlength: 500
            },
            "siglas_equi": {
                required: true,
                minlength: 2,
                maxlength: 25
            },
            "fundacion_equi": {
                required: true,
                digits: true
            },
            "region_equi": {
                required: true,
                minlength: 2,
                maxlength: 25
            },
            "numero_titulos_equi": {
                required: true,
                digits: true
            }
        },
        messages:{
            "nombre_equi": {
                required: "Por favor, ingrese el nombre del equipo",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 500 caracteres"
            },
            "siglas_equi": {
                required: "Por favor, ingrese las siglas del equipo",
                minlength: "Las siglas deben tener al menos 2 caracteres",
                maxlength: "Las siglas no pueden tener más de 25 caracteres"
            },
            "fundacion_equi": {
                required: "Por favor, ingrese el año de fundación del equipo",
                digits: "Ingrese un año válido"
            },
            "region_equi": {
                required: "Por favor, ingrese la región del equipo",
                minlength: "La región debe tener al menos 2 caracteres",
                maxlength: "La región no puede tener más de 25 caracteres"
            },
            "numero_titulos_equi": {
                required: "Por favor, ingrese el número de títulos del equipo",
                digits: "Ingrese un número válido"
            }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>
