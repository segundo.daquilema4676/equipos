<!-- Vistas de Cliente -->

<!-- index.php -->
<h1>
  <i class="fa fa-futbol"></i>
  EQUIPOS
</h1>
<div class="row">
  <div class="col-md-12 text-end">

    <!-- Button trigger modal -->
    <a href="<?php echo site_url('equipos/nuevo');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR EQUIPO
    </a>
    <br><br>
  </div>
</div>
<?php if ($listadoEquipos): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>NOMBRE</th>
      <th>SIGLAS</th>
      <th>FUNDACIÓN</th>
      <th>REGIÓN</th>
      <th>NÚMERO DE TÍTULOS</th>
      <th>ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoEquipos as $equipo): ?>
    <tr>
      <td><?php echo $equipo->id_equi; ?></td>
      <td><?php echo $equipo->nombre_equi; ?></td>
      <td><?php echo $equipo->siglas_equi; ?></td>
      <td><?php echo $equipo->fundacion_equi; ?></td>
      <td><?php echo $equipo->region_equi; ?></td>
      <td><?php echo $equipo->numero_titulos_equi; ?></td>
      <td>
        <a href="<?php echo site_url('equipos/editar/').$equipo->id_equi; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>

        <a href="<?php echo site_url('equipos/borrar/').$equipo->id_equi; ?>" class="btn btn-danger" title="Eliminar">
          <i class="fa fa-trash"></i>
          Eliminar
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php else: ?>
<div class="alert alert-danger">
  No se encontraron equipos registrados
</div>
<?php endif; ?>

<!-- nuevo.php -->
<br>


<script type="text/javascript">
    $("#formulario_equipo").validate({
        rules:{
            "nombre_equi": {
                required: true,
                minlength: 2,
                maxlength: 500
            },
            "siglas_equi": {
                required: true,
                minlength: 2,
                maxlength: 25
            },
            "fundacion_equi": {
                required: true,
                digits: true
            },
            "region_equi": {
                required: true,
                minlength: 2,
                maxlength: 25
            },
            "numero_titulos_equi": {
                required: true,
                digits: true
            }
        },
        messages:{
            "nombre_equi": {
                required: "Por favor, ingrese el nombre del equipo",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 500 caracteres"
            },
            "siglas_equi": {
                required: "Por favor, ingrese las siglas del equipo",
                minlength: "Las siglas deben tener al menos 2 caracteres",
                maxlength: "Las siglas no pueden tener más de 25 caracteres"
            },
            "fundacion_equi": {
                required: "Por favor, ingrese el año de fundación del equipo",
                digits: "Ingrese un año válido"
            },
            "region_equi": {
                required: "Por favor, ingrese la región del equipo",
                minlength: "La región debe tener al menos 2 caracteres",
                maxlength: "La región no puede tener más de 25 caracteres"
            },
            "numero_titulos_equi": {
                required: "Por favor, ingrese el número de títulos del equipo",
                digits: "Ingrese un número válido"
            }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>
