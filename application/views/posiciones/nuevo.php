<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVA POSICIÓN
  </b>
</h1>
<br>

<form class="" action="<?php echo site_url('posiciones/guardarPosicion');?>" method="post" enctype="multipart/form-data" id="formulario_posicion">
  <div class="row">
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          <br>
          <label for="">NOMBRE:</label>
          <input type="text" name="nombre_pos" id="nombre_pos" class="form-control" placeholder="Ingrese el nombre">
        </div>
      </div>

      <br>
      <label for="">DESCRIPCIÓN:</label>
      <textarea name="descripcion_pos" id="descripcion_pos" class="form-control" placeholder="Ingrese la descripción"></textarea>
      <br>
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save"></i> GUARDAR</button>
          <a href="<?php echo site_url('posiciones/index');?>" class="btn btn-danger"> <i class="fa fa-ban"></i>
            CANCELAR
          </a>
        </div>
      </div>



    </div>

    <div class="col-md-6">
      <br>
      <div class="row">
        <div class="col-md-12">
          <br>
          <img src="https://mifutbolecuador.futbol/wp-content/uploads/2023/02/liga-pro-2023-equipos.png?w=1000" style="width: 500px;" alt="">
          <br>
          <br>
          <br>

        </div>
      </div>
      <br>
    </div>
  </div>
</form>
<br><br>


<script type="text/javascript">
    $("#formulario_posicion").validate({
        rules:{
            "nombre_pos": {
                required: true,
                minlength: 2,
                maxlength: 150
            },
            "descripcion_pos": {
                required: true,
                minlength: 5
            },
            "latitud": {
                required: true
            },
            "longitud": {
                required: true
            }
        },
        messages:{
            "nombre_pos": {
                required: "Por favor, ingrese el nombre de la posición",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 150 caracteres"
            },
            "descripcion_pos": {
                required: "Por favor, ingrese la descripción de la posición",
                minlength: "La descripción debe tener al menos 5 caracteres"
            },
            "latitud": {
                required: "Por favor, ingrese la latitud de la posición"
            },
            "longitud": {
                required: "Por favor, ingrese la longitud de la posición"
            }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>
