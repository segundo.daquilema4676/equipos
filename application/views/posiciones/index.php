<h1>
  <i class="fa fa-map-marker"></i>
  POSICIONES
</h1>
<div class="row">
  <div class="col-md-12 text-end">
    <a href="<?php echo site_url('posiciones/nuevo');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR POSICIÓN
    </a>
    <br><br>
  </div>
</div>
<?php if ($listadoPosiciones): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>NOMBRE</th>
      <th>DESCRIPCIÓN</th>
      <th>ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoPosiciones as $posicion): ?>
    <tr>
      <td><?php echo $posicion->id_pos; ?></td>
      <td><?php echo $posicion->nombre_pos; ?></td>
      <td><?php echo $posicion->descripcion_pos; ?></td>
      <td>
        <a href="<?php echo site_url('posiciones/editar/').$posicion->id_pos; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>
        <a href="<?php echo site_url('posiciones/borrar/').$posicion->id_pos; ?>" class="btn btn-danger" title="Eliminar">
          <i class="fa fa-trash"></i>
          Eliminar
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php else: ?>
<div class="alert alert-danger">
  No se encontraron posiciones registradas
</div>
<?php endif; ?>
