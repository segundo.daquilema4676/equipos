<h1>EDITAR POSICIÓN</h1>
<form method="post" action="<?php echo site_url('posiciones/actualizarPosicion'); ?>" enctype="multipart/form-data" id="formulario_posicion">
  <input type="hidden" name="id_pos" id="id_pos" value="<?php echo $posicionEditar->id_pos; ?>">
  <label for=""><b>Nombre:</b></label>
  <input type="text" name="nombre_pos" id="nombre_pos" value="<?php echo $posicionEditar->nombre_pos; ?>" placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for=""><b>Descripción:</b></label>
  <textarea name="descripcion_pos" id="descripcion_pos" placeholder="Ingrese la descripción..." class="form-control" rows="3" required><?php echo $posicionEditar->descripcion_pos; ?></textarea>
  <br>
  
  <br>
  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
      <a href="<?php echo site_url('posiciones/index'); ?>" class="btn btn-danger"> <i class="fa fa-times"></i> &nbsp Cancelar</a>
    </div>
  </div>
</form>
<br>
<br>

<script type="text/javascript">
    $("#formulario_posicion").validate({
        rules:{
            "nombre_pos": {
                required: true,
                minlength: 2,
                maxlength: 150
            },
            "descripcion_pos": {
                required: true,
                minlength: 5
            }
        },
        messages:{
            "nombre_pos": {
                required: "Por favor, ingrese el nombre de la posición",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 150 caracteres"
            },
            "descripcion_pos": {
                required: "Por favor, ingrese la descripción de la posición",
                minlength: "La descripción debe tener al menos 5 caracteres"
            }
        },
        errorClass: "text-danger" // Establecer la clase de estilo para los mensajes de error
    });
</script>
