<!DOCTYPE html>
<html lang="en">
<head>
	<title>Coop Daquilema</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="" />
<meta name="keywords" content="">
<meta name="author" content="Phoenixcoded" />
<!-- Favicon icon -->
<link rel="icon" href="<?php echo base_url('static/img/logo.svg'); ?>" type="image/x-icon">
<!-- vendor css -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<!-- api de google-->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCTqeKCayVcz2foCkSUk4672vXvjT2ohEQ&libraries=places&callback=initMap">
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<title>MVC1</title>
<!-- Incluye jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Incluye el plugin jQuery Validate -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<!-- importacion de swwtaler -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
<script src="<?php echo base_url('static/librerias/jquery-validate/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('static/librerias/jquery-validate/additional-methods.min.js'); ?>"></script>

<script src=" https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.all.min.js "></script>
<link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.min.css " rel="stylesheet">

</head>
<body class="">
	<!-- [ Pre-loader ] start -->
	<div class="loader-bg">
		<div class="loader-track">
			<div class="loader-fill"></div>
		</div>
	</div>
	<!-- [ Pre-loader ] End -->
	<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar menu-light ">
		<div class="navbar-wrapper  ">
			<div class="navbar-content scroll-div " >

				<div class="">
					<div class="main-menu-header">
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMsAAAD5CAMAAAC+lzGnAAAA3lBMVEUAIlXTqnX/////zgDbsHfXrXYAHVTlBzo+QFoAIFXbuo/WrHYAG1Tat4sAGFQADFPMpnQAFVNRUF6FdmjSqXfasGL4yCfYs4V9cWcRJ1adhmy5mnLHo3SRemfQpWvSsHj17eEACVJbWWEAElNoYmPWj2yQfmviOUby5daWfWitjm3Bn3PvwS7pvD11bGXaZ1veQE0nN1kfMVj69e/q17/ky6zy5tiojW1tZmQAAFFKTF0wPVsOK1eEcGVkW2BYVWDfwp7mz7PasGHUn3HluUnZdGDcWFVHRlsOMFkmOlouukosAAAUUElEQVR4nO2da3viOpKAbdbSoMhxzGUmYBtOGlhvwDPsAknDwTSkZ3aHzP//QyvJJli25Dt0Mof61E/H2HpdqlJVSZaUhlBWj3efV+6fxI1WxP/9NFQ/rwyf/+As3brk1AqQJviSLH/++5/rkb+HWgajSYpYAcw//jNN/lGOpfvX/6hL/hZoBv+uy+VhDug1v/33n9Lkf377JCzgmyIX48ZyY7mx3FhuLDeWG8u/E0tqDDO+KMvf/lKX/G+ol/ZILgeLsaj/919p8s/iLKthnSH/R9CvpQkIL/otVWic/F3C8iSU50+di72IG610h0L51e1NFXGTH5XHX92w2uT+348l1Sa/gpxZzEXza8tCO7P8Dr+2/B5heUiJLb6CfLuxfEq5sXxOubF8TrmxfE65sXxOubF8TslmQejKTcoWBIX/ncmCmkvjc9EgY3cUwmSyGD1vDg34aXCQYUxsqyX6Uw4WTTM7TShW67UF6Yu5o4HSLEBVNW/aHP9y3SA4XnRsDauVWFSMu9a2r/9KGqSjpmti2ppqLPQGwNorv4wG6fDgqqemVGUhtzDtEfolNEjXjzPz3JDqLOQmmrPxr+6ikb7cUzNRa2UhhgPswfs13QBCLTS3Va4RNbGQG2HT9a9mOBAtek588VJtLMxwrLZ+BRoEW03X0xLLsOpkoTTr7aVpiMH704RO6mchhqPZk0tGakjvHyxN/OzyLJrwfnT8tOdK6zJhNNKN0doUk5AwpCSL3rZV2bpBov+5f4FIjTjhkYelTzVn23JxsqL3JzP5KkjNcZv1RtEIGcueLX0iUNdkwBb+MkdeifRdim4wcNZ+fdEAgnDX8VRZ78Lq+igNC3PlyEhvbS1p7yWR2npfU9wJ4dE15TpxrGZL3gly5vvkxbddTf6Q7mw/ruzUkP7QtBypSoDmblONM3ftAkFj6XpSGgy8zaKSbkhXPqyBzOLJ/d1lhmEWqcOglt8RjMKnp2l2ZzGOPwzJhb/OUCZr+ZvSvI7fynpTxWpKEC7nXoqLdtxFrIl9uXDX7fby+2Ls9JY5XH/B+hhxmGhjS90McdEKBzNe2zLxotiwI9d37iG5eK2PuP+RLX1yjOWBWJhENJ5F8npI79ovcnqVMnVLEmBEs7x0Fvkom4OFZLKH/CFsuRosy76B6PF1smCtWIWhbD2ZVkUsM+lB62PB3XW72AhcvjaO4NgfePEgui4Wku5txwXHq0p1fgj9jsm7gXpYALCOijiAvBQLdWr6xqudxZmWKo9Unn9B44NZMwtwM4f4y7CQZM2pm6VTLr2rzgL/oCz5xv0vweLNImIvr8+SFdXlZwGD38cRiV5YjEXSpOz5yp2SfuciLMJSUGEWiJZCmOya0ny6NdLufW0W1Gq6rlGKxehpjuW35OHEVVlIpr4ggVOFGizQrIM0zLsiC9KVLa2gVKsnA3N9MMQ0V2MhaVNY2apaG8eqd9iJaK7EgvTddhaWBKrX+QGw57tkunoVFr5MU8ecBVbtOdRjD7oCC9L1SbRgUs/8C9CcXmzy9eIsyFjMPW4mpq65JAy8zjKaXVyWBaHWLlH1r29ejMSFnea5pnBRFoj8norjLah1jg+YNBhAl2ZBRtN1BNNyNc9XslkXdFEWhJaWcOq19rlX4gZ6RVmmhVj0ibACV4VFWukFncvqRZe9Rayty7GgZcdLGN+JBV2UZS58LJ2JWZSL+UmMvezZoon2X8KSNhOTJ0dG+nIimB25eB9LsmDszOUzMfnyfWQYo1l81kWoF3PJvbRa7YWEUBtkyHP2vLULNpvI9zQhC7Y3MBLj1MiCNXuUvmglfx2GLrXhZnmFfYyYpjk5T8LW1sfyLCYqNPcKjW0nslIw0Atft2Q0du+UG9TEAoCbY06pYH0MombHDF20WC/sL6o337EQpw4WjM3pMc/KjqK1PgTH7z+C6UqhvZx0o3k9Os1YA4tmDxb5qv4l6pbERc/pNH8aS9DTUOtbRRagmT0/nv3VyBKs7rJNTdrHwiYBdb7MYDmtOBL7MU21J3r+pSkl68lI7+9nqXoJWmVH52YSLCSL7+11OYs9EdQXamdhA86S/SONhQ8WYnEygruNB34YMha0XBabsKxQ50eiXCxNOL2g1qJHsngsZyn8FVHddf6cLLCFejb17Sl6SZHSdX7JF00VWJDe7IW7P5ZhId27ZJ1/Mz2mTrQXYRkrrLKyGHgfBcTCLMjo9zql6/wkFEpZABFjSdlsE1g7HUK0cCPrNYqy0PzDqZLvk6R0spPNU8fyl6knyKBOjbR7zeOAWxWUZvsikt2GBB0V6/yqPVfESwRjLLSgJas5EKV5sfWURViQPh6xUmzVOgzQQOcocgPxvBKybzykK9vidLlZ6PAcrlur47skx2omn5LMkVFrOVlLV8vF2PKyQGO0Pum7ljo/EDgQUb5P3mFbtlqunF7Qv2ZnndZS68ODfCysb7dTlmcXZtEndX9jlZ+FPl45WvJVpoVZoj+6OgvN25RZKgzJ4oNPiz89C5Gxm8ICtOkh9IxfgaUlZcHEK/rwFE58aRbgDaKfTHxdFgDMjs+Nu1+PJZydx3Yvvqj9y7Fg26RxqjdJLmr/cixggCzTHonyh0/Ckrp2NMYyhkdFWFn5JCxF9NJCkgLkV2QRgtxYbiw3lhvLjeXGcmO5sdxYbiw3lhvLjeUPwyKcs0j57pVnKbam99IsPyrppQKL0cvNcv/pWQbZ8y8PtIM8Kndylnl99lKBpTWNXuBmsWjCM5n0UeTjANELuQ4LtKIX9IRrFRjLvfJGWcbCp7Q5luQGAVdhQYoV7WMb4T4FD/RvdwGLcBUPPEZYsJVcVnMdluUswqIdhCzM9u+Un0NyhXDFDGpGmorX77+IxbejLOK91MaU5U15oSz/ErIs7fNjgJ38KqBAPbk8C9xGB7GuYJkBXTREWV4ZCxDSon7U6szkNVcZ9/V91GoFb5S29J1cM/ypfKcse2EvhFxzktdcpY9xa8jxWmgN0KcsLwo94U3i6YwfUQ8y/zUsBueSB8LJAr0N6AlqyqorHU316D4jeJ3gvUofa0XNRRR9UN4NZXlSViSIwTPhgje0cKKKSSwju4ZemCl8iDkSGkNrQO+xUhpv1KSSDpfex4g4MlXz48+5BgsXfGBP/ImVQYege8LyOiTX+MJr+FAoYVTXiPnH0SZgT9x/dvSdvxEW6pRN8XDKvxQ7Pkt3BXsJmpnxSmDTw+rwtUFYVKGTYndaci8lPk5doY9x7kfVxOYSXPRCWJ6oI+sIIzLELTbCceAr6IXfOQqIdyEJhqBnehbnI3W4feFVsJc2UF1eL3wXw2vxG0f0Ud0VZWGZpYR4H2msimOe7PIssBlVi9YT74bRXxM39sjOSH2l0aX4zAsuvEwkdZfvY63ooK86bUkj6R/fGAs9DxVshAOqYkyji1pNvpNdnAXtuEF/Jl7MD7csGmMs9JxaPBOmliTS4Rbozjnii/cxYx59uKisxa7qsAgmOIeXpvyOcORXEB8OzTizujQLn1KqQJhlkejEC8yFsfyUj5ZKi98akHPwl+5j+ohDscVdB/m0FW8hyzN7mpgFNjnF2NEA88IsqMV97AAm4hayjsiOf6YsT2yEEXtlPrlUtXbkhhfuY8YhGiLLYkaEaAMfn07nVtNajCr2yvEhxo74kiJ6eYicHM49SL6nys7muthA/MlXUNu4+ziD+0WeWxLu6BBDsqHIEtb8axStbfss21wskHNiqil71yOW63+wMK/sSQ6UNzbc3ibm4ryFS/61o6ZzFtOOBkwyFtTnrWUt+d5ybIUe+XQ2Oi32J3Ot000dTtfrfHvdyNdaAy8Pi8H/Hshap9A33Y2c8846WUdin1yASeTDn5RdN56HxWjz/OKwklw3AacuFrKwuN8WezKS9vMW85HHXJAlNkxKhz+SldDrniIsgScTV8likxf0HY1rZdHAh2huyDKeckaKp5KVp0Ea9taIsrDk0pLoEemcxZD0LuiN8BA9iNzsl2JB283kQzbH4C/Gkf+xtpBYS1COfOFYaJVMav2xYOKscfjw7SwP/IvIy6JAPTH0QJ//5g90xOEL6f60YSQNi7I03qj1u7Kf7NaAb80yc5OA3CyCx/V5Y5EN+eQhzC2FXeyDhZZisSOxfkXniu3UT2Q0pwoL4mbCqMgMGe1oiBwMLhEWFvirPdnu2K0OvxkYtrJgSrMg1IntBTeVfXirs9DgpJYzC7V+LHPL8eCIiPWQDlOWBY0n/OXAERcrWQaPg4wyxrJik7CSVJkmmHwvUzU3XTMtV5McmwDSWBDiQyb50HKKru5WCRY29mNHtssEgomToNzUfRyMnic7z0JSwGKPMebd2DvryHZVQYbDqSXC0mAKncsUg1rxjwwlBcLT9SnnjEh/BpXEWXO2dA+PcAhviFhYUCaeRGPP8eM7iIOOZMuGACbn+S8R0ZdxD0bcsfQr+AUNpCNqibKsaLScKLRGYEbd+FfTVur+ygUFGYv428JSd0w3XqEX36+ELMyVqV2pYpRW24xrxm4WPuNAigI38dur6kYyepM3u2AXRNTCsbBARjr4K3QHjASMN8rwzXlFR27iE38stXviutnEzGNDwsIsRjXb0leNjJjvJzCqW8fZfMg42onxCHTknlLf0xc//C5lYYM/kJQ6mTz04g9UNftQ+YQxvT9PfiUPrMR5Umf2BRsm3xpylmd2j7m0AEScUy/ZE0xLuFV0boG6L9haAojn8gMJvtzufk9hYTV/FaQYNHqYCN6gOS/f0Uj3cgVni5GsWO4j9QNtQ5gay1hoRUa2uuF0n30SBmP7kLJhfBpJ630gOvANWCltgEHWjlepLIH5E6NLebruJ30npWkX72n0YG1VdOAbHqQMw6ek4HsjnYWFmKp6TBs2jKZozwSM15NCB3Qi2Fr+SHov1mc7crMn49ye/ei1kcHSeGLtstN6mQJ38VgjaELX7i2RnmuHPQR15FuOcL9ZbLbTXgmbAj9nxiksYS8TLOOLtkTvCXcawQCs50cl5XTG8PeG0pzPgHgHJjxLdSRoGfSK50TLkyyNOwqj7uWOmd4QtSXbppD0ZNbZwrFB9JN8u+S/oN4aLzYzT7JdEdZ6u9Qg7xsb8IeJHiZkYTGmiv3USAuRmFZySiPLt9adw3HRVwyDFldC0Q1DWfrHievQ4+2EvyVB0SH1yCQEg0j/LdluEQubjKWazthJDYn72Uk9mmpb1uDHZHRob49NIu3JfGCtPSA5LjhQipX+Cml+y95CwlgkLKzApGJZ6e/8io4z6ZG57B0T88Fd0zQdVuY3uyqQmMjpB868n/ECl0HZ8UXUbCFL4Jjl2ekHTKsnPdi0uADTes84kiu0+/iAn8YSOGZVncgS5jNNU3bCdFHB2qyd6jvZ41yqleGdsNESlsb34AH7LBgFGn6ejaAySdRZW3IySwRFD6p09yJjkbMEOSY2t5kwdHswK+eWYzIB2myTfcQjCpfEdp8kbZaxsEl/4iG32TkwPTNnLTgEMq9KSLgg3KEk/hhjw64fJgfJLJYg/MdOnoQe6ePmVDb4ZZF41jazd1ExgihM7MIyWFbMMwN7kas6AaG/sbWCOGQQ8ubNfMd4G8HAIhrvs1nCYSZ3qQUZD75ly4/RToKYtuWPcx6v3joEHkbsjbNZGq8qg/GzHUAg0NgdOmucrR5ABv91Z7/MW15DRtvJRElnCRwAybJyJ1kk4u8v2q7nmOS9i6NgoJqO7bYXfZj/rsYoB0oGS+Mne7w5KpAxIkQC4d1hMrDspH40Z2bNt+9jklAXuKOyUeWRS26W0DWbk3GxYivSdaPv7y3ugHismfPjUrKJqVwgctnP5c44J0vjhc0haOuM+FXEQ1z1u/uxEgR3Z5MHwlG0JqAvZmy0f5QNkflZGs8MBnijtBRcymM0rSAPBvZevNFVxg1aRzYjN5QFLoVY2PIy6noy8j2JQHQk6Tnu9oqd3nz6tbJXpblXCZZw1FTBbFvmlFyagZKfltGqQgJXZm9pI2QxlnDUpKmS9ASWNIG7ybLM1AYyDl5W3FKcJfQAWFsvyvSUfFFKXIg+WW40vMu0+kIsxGgC1dijVo1TYSmCWgeb+a/hW7bVF2NprF6D6ANbpVRTlETfdcIsIl//KsRy6mfEanqwtok9GUprHxT0cvevgiynfobxupn3AJByJMRSzAL+qwxLGJ7R+ND1L9fR9N0miO+H91lRSxWWxvfuMOhoXq/cfEuWIP1hEh70OHzNa/TlWBqrn92wo3n7UiN5BgnanuqHRSylHAtRzVuoGmCNyg0cUhJoHN0gZBk+5ndfFVjCGVqWCtiH+mgQ1BfrU6kt95hSlYX4gMdQN5pdV0/T4TGMqIkjLmbz1VgaT2FHIz1ivR+nnC6XUyf6w9YKY69h96WMUsqzUJrQpREvsPErjZ5QX+5n4YzM8DE9p78IC6F5VUMazRu0C2XwEY3QOmF4bFkVnVRkobo55b/AnI36xf0ALdtsLeejfluFpCILcdCvoReghQl3/16kLkFc8Puh83GW8/C+GkllFjJ4vpxo6FEx06Oeq7PRwpNxnJ7LnNV6V00sRJ5PboDO6pnuYYHS65FINxT/4BKOk0q6b+W8MC91sNCupp6UQw+Fn258g6pHOCVujPvtH1ZkVoB0rsLhilDqYaFd7f7UNlpmdWz34O+Itz1P8pNxXUf9ZXMzc8xIfbYelTCpi4XI08tbVDua5li9/XHRR2NDJ2raNfdzd6Zq4DzrNFTfqlvJWWpkIcp5Io5gGNEPUD17NrN67c10ZntdECkwD4fDu5enGklqZqHy9PPuXo3yYMw+OeLK/kP17jW+Yqq61M5CZPX88z6inrgM8dvL91oVEsolWKisnt8euyoPRPi6j3c/L8JB5VIsVJ6eX97uiAENqXGoj3evP2tyvhK5JAuV1erp+fX15flptbqUOj7k/wH5eiFMzrq+VwAAAABJRU5ErkJggg==" alt="" style="width: 200px;">
						<div class="user-details">
							<div id="more-details">Opciones: <i class="fa fa-caret-down"></i></div>
						</div>
					</div>
					<div class="collapse" id="nav-user-link">
						<ul class="list-unstyled">
							<li class="list-group-item"><a href="user-profile.html"><i class="feather icon-user m-r-5"></i>View Profile</a></li>
							<li class="list-group-item"><a href="#!"><i class="feather icon-settings m-r-5"></i>Settings</a></li>
							<li class="list-group-item"><a href="<?php echo base_url(); ?>"><i class="feather icon-log-out m-r-5"></i>Logout</a></li>
						</ul>
					</div>
				</div>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
<br>
				<ul class="nav pcoded-inner-navbar ">

											<li class="nav-item pcoded-menu-caption">
											    <label>Menu</label>
													<li class="nav-item">
													    <a href="<?php echo site_url('posiciones/index');?>" class="nav-link "><span class="pcoded-micon"><i class="fa fa-bank"></i></span><span class="pcoded-mtext">Posición</span></a>
													</li>

														<li class="nav-item">
															<a href="<?php echo site_url('equipos/index');?>" class="nav-link "><span class="pcoded-micon"><i class="fa fa-credit-card"></i></span><span class="pcoded-mtext"></span>Equipos</a>

														</li>
													</li><li class="nav-item">
													    <a  href="<?php echo site_url('jugadores/index');?>" class="nav-link "><span class="pcoded-micon"><i  class="fa fa-store"></i></span><span class="pcoded-mtext">Jugadores</span></a>
													</li>
											</li>


					<li class="nav-item pcoded-menu-caption">
					    <label>Maps</label>
					</li>

				</ul>
			</div>
		</div>
	</nav>
	<!-- [ navigation menu ] end -->
	<!-- [ Header ] start -->
	<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">


				<div class="m-header">
					<a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
					<a href="#!" class="b-brand">
						<!-- ========   change your logo hear   ============ -->
						<img src="assets/images/logo.png" alt="" class="logo">
						<img src="assets/images/logo-icon.png" alt="" class="logo-thumb">
					</a>
					<a href="#!" class="mob-toggler">
						<i class="feather icon-more-vertical"></i>
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a href="#!" class="pop-search"><i class="feather icon-search"></i></a>
							<div class="search-bar">
								<input type="text" class="form-control border-0 shadow-none" placeholder="Search hear">
								<button type="button" class="close" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</li>
					</ul>
					<ul class="navbar-nav ml-auto">
						<li>
							<div class="dropdown">
								<a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="icon feather icon-bell"></i></a>
								<div class="dropdown-menu dropdown-menu-right notification">
									<div class="noti-head">
										<h6 class="d-inline-block m-b-0">Notifications</h6>
										<div class="float-right">
											<a href="#!" class="m-r-10">mark as read</a>
											<a href="#!">clear all</a>
										</div>
									</div>
									<ul class="noti-body">
										<li class="n-title">
											<p class="m-b-0">NEW</p>
										</li>
										<li class="notification">
											<div class="media">
												<img class="img-radius" src="assets/images/user/avatar-1.jpg" alt="Generic placeholder image">
												<div class="media-body">
													<p><strong>John Doe</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>5 min</span></p>
													<p>New ticket Added</p>
												</div>
											</div>
										</li>
										<li class="n-title">
											<p class="m-b-0">EARLIER</p>
										</li>
										<li class="notification">
											<div class="media">
												<img class="img-radius" src="assets/images/user/avatar-2.jpg" alt="Generic placeholder image">
												<div class="media-body">
													<p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>10 min</span></p>
													<p>Prchace New Theme and make payment</p>
												</div>
											</div>
										</li>
										<li class="notification">
											<div class="media">
												<img class="img-radius" src="assets/images/user/avatar-1.jpg" alt="Generic placeholder image">
												<div class="media-body">
													<p><strong>Sara Soudein</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>12 min</span></p>
													<p>currently login</p>
												</div>
											</div>
										</li>
										<li class="notification">
											<div class="media">
												<img class="img-radius" src="assets/images/user/avatar-2.jpg" alt="Generic placeholder image">
												<div class="media-body">
													<p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
													<p>Prchace New Theme and make payment</p>
												</div>
											</div>
										</li>
									</ul>
									<div class="noti-footer">
										<a href="#!">show all</a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="dropdown drp-user">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="feather icon-user"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right profile-notification">
									<div class="pro-head">
										<img src="assets/images/user/avatar-1.jpg" class="img-radius" alt="User-Profile-Image">
										<span>John Doe</span>
										<a href="auth-signin.html" class="dud-logout" title="Logout">
											<i class="feather icon-log-out"></i>
										</a>
									</div>
									<ul class="pro-body">
										<li><a href="user-profile.html" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
										<li><a href="email_inbox.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li>
										<li><a href="auth-signin.html" class="dropdown-item"><i class="feather icon-lock"></i> Lock Screen</a></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>


	</header>
	<!-- [ Header ] end -->



<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">

                </div>
            </div>
        </div>

        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <!-- Latest Customers start -->
            <div class="col-lg-12 col-md-12">
                <div class="card table-card review-card">
                    <div class="card-header borderless ">
                        <h5>DaquiSystem</h5>
                        <div class="card-header-right">
                            <div class="btn-group card-option">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-more-horizontal"></i>
                                </button>
                                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                    <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                    <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                    <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                    <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="review-block">
                            <div class="row">
