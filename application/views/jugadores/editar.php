<h1>
  <b>
    <i class="fa fa-edit"></i>
    EDITAR JUGADOR
  </b>
</h1>
<br>

<form class="" action="<?php echo site_url('jugadores/actualizarJugador');?>" method="post" enctype="multipart/form-data" id="formulario_editar_jugador">
  <input type="hidden" name="id_jug" value="<?php echo $jugadorEditar->id_jug; ?>">
  <div class="row">
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-6">
          <br>
          <label for="apellido_jug">APELLIDO:</label>
          <input type="text" name="apellido_jug" id="apellido_jug" class="form-control" placeholder="Ingrese el apellido" value="<?php echo $jugadorEditar->apellido_jug; ?>">
        </div>
        <div class="col-md-6">
          <br>
          <label for="nombre_jug">NOMBRE:</label>
          <input type="text" name="nombre_jug" id="nombre_jug" class="form-control" placeholder="Ingrese el nombre" value="<?php echo $jugadorEditar->nombre_jug; ?>">
        </div>
      </div>

      <br>
      <label for="estatura_jug">ESTATURA (m):</label>
      <input type="number" name="estatura_jug" id="estatura_jug" class="form-control" step="any" placeholder="Ingrese la estatura" value="<?php echo $jugadorEditar->estatura_jug; ?>">
      <br>

      <label for="salario_jug">SALARIO:</label>
      <input type="number" name="salario_jug" id="salario_jug" class="form-control" step="0.01" placeholder="Ingrese el salario" value="<?php echo $jugadorEditar->salario_jug; ?>">
      <br>

      <label for="estado_jug">ESTADO:</label>
      <select name="estado_jug" id="estado_jug" class="form-control">
        <option value="Activo" <?php if ($jugadorEditar->estado_jug == 'Activo') echo 'selected'; ?>>Activo</option>
        <option value="Inactivo" <?php if ($jugadorEditar->estado_jug == 'Inactivo') echo 'selected'; ?>>Inactivo</option>
      </select>
      <br>

      <label for="id_pos">POSICIÓN:</label>
      <select name="fk_id_pos" id="fk_id_pos" class="form-control">
        <?php foreach ($posiciones as $posicion): ?>
          <option value="<?php echo $posicion->id_pos; ?>" <?php if ($posicion->id_pos == $jugadorEditar->fk_id_pos) echo 'selected'; ?>><?php echo $posicion->nombre_pos; ?></option>
        <?php endforeach; ?>
      </select>
      <br>

      <label for="id_equi">EQUIPO:</label>
      <select name="fk_id_equi" id="fk_id_equi" class="form-control">
        <?php foreach ($equipos as $equipo): ?>
          <option value="<?php echo $equipo->id_equi; ?>" <?php if ($equipo->id_equi == $jugadorEditar->fk_id_equi) echo 'selected'; ?>><?php echo $equipo->nombre_equi; ?></option>
        <?php endforeach; ?>
      </select>
      <br>

    </div>

    <div class="col-md-6">
      <br>
      <img src="https://static.vecteezy.com/system/resources/previews/010/135/398/original/cartoon-football-soccer-player-man-in-action-png.png" style="width: 800px;"alt="">
      
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save"></i> ACTUALIZAR</button>
          <a href="<?php echo site_url('jugadores/index');?>" class="btn btn-danger"> <i class="fa fa-ban"></i> CANCELAR</a>
        </div>
      </div>
      <br>
    </div>
  </div>
</form>
<br><br>

<script type="text/javascript">
    $("#formulario_editar_jugador").validate({
        rules:{
            "apellido_jug": {
                required: true,
                minlength: 2,
                maxlength: 500
            },
            "nombre_jug": {
                required: true,
                minlength: 2,
                maxlength: 500
            },
            "estatura_jug": {
                required: true,
                number: true,
                min: 0
            },
            "salario_jug": {
                required: true,
                number: true,
                min: 0
            },
            "estado_jug": {
                required: true
            },
            "id_pos": {
                required: true
            },
            "id_equi": {
                required: true
            }
        },
        messages:{
            "apellido_jug": {
                required: "Por favor, ingrese el apellido del jugador",
                minlength: "El apellido debe tener al menos 2 caracteres",
                maxlength: "El apellido no puede tener más de 500 caracteres"
            },
            "nombre_jug": {
                required: "Por favor, ingrese el nombre del jugador",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 500 caracteres"
            },
            "estatura_jug": {
                required: "Por favor, ingrese la estatura del jugador",
                number: "Ingrese un valor numérico válido",
                min: "La estatura no puede ser negativa"
            },
            "salario_jug": {
                required: "Por favor, ingrese el salario del jugador",
                number: "Ingrese un valor numérico válido",
                min: "El salario no puede ser negativo"
            },
            "estado_jug": {
                required: "Por favor, seleccione el estado del jugador"
            },
            "id_pos": {
                required: "Por favor, seleccione la posición del jugador"
            },
            "id_equi": {
                required: "Por favor, seleccione el equipo del jugador"
            }
        },
        errorClass: "text-danger"
    });
</script>
