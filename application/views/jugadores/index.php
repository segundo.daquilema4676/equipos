<h1>
  <i class="fa fa-user"></i>
  JUGADORES
</h1>
<div class="row">
  <div class="col-md-12 text-end">

    <a href="<?php echo site_url('jugadores/nuevo');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR JUGADOR
    </a>
    <br><br>
  </div>
</div>
<?php if ($listadoJugadores): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>APELLIDO</th>
      <th>NOMBRE</th>
      <th>ESTATURA</th>
      <th>SALARIO</th>
      <th>ESTADO</th>
      <th>POSICIÓN</th>
      <th>EQUIPO</th>
      <th>ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoJugadores as $jugador): ?>
    <tr>
      <td><?php echo $jugador->id_jug; ?></td>
      <td><?php echo $jugador->apellido_jug; ?></td>
      <td><?php echo $jugador->nombre_jug; ?></td>
      <td><?php echo $jugador->estatura_jug; ?></td>
      <td><?php echo $jugador->salario_jug; ?></td>
      <td><?php echo $jugador->estado_jug; ?></td>
      <td>
  <?php
  if ($jugador->fk_id_pos) {
    $posicion = $this->Posicion->obtenerPorId($jugador->fk_id_pos);
    echo $posicion ? $posicion->nombre_pos : 'N/A';
  } else {
    echo 'N/A';
  }
  ?>
</td>
<td>
  <?php
  if ($jugador->fk_id_equi) {
    $equipo = $this->Equipo->obtenerPorId($jugador->fk_id_equi);
    echo $equipo ? $equipo->nombre_equi : 'N/A';
  } else {
    echo 'N/A';
  }
  ?>
</td>

      <td>
        <a href="<?php echo site_url('jugadores/editar/').$jugador->id_jug; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>
        <a href="#" class="btn btn-danger eliminar-jugador" data-id="<?php echo $jugador->id_jug; ?>">
          Eliminar
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<script>
  // JavaScript para manejar la eliminación del jugador
  document.addEventListener('DOMContentLoaded', function () {
    const eliminarLinks = document.querySelectorAll('.eliminar-jugador');

    eliminarLinks.forEach(link => {
      link.addEventListener('click', function (event) {
        event.preventDefault();
        const idJugador = this.getAttribute('data-id');
        const confirmar = confirm('¿Está seguro de que desea eliminar este jugador?');
        if (confirmar) {
          window.location.href = "<?php echo site_url('jugadores/borrar/'); ?>" + idJugador;
        }
      });
    });
  });
</script>
<?php else: ?>
<div class="alert alert-danger">
  No se encontraron jugadores registrados
</div>
<?php endif; ?>
