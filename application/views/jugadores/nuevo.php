<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVO JUGADOR
  </b>
</h1>
<br>

<form class="" action="<?php echo site_url('jugadores/guardarJugador');?>" method="post" enctype="multipart/form-data" id="formulario_jugador">
  <div class="row">
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-6">
          <br>
          <label for="apellido_jug">APELLIDO:</label>
          <input type="text" name="apellido_jug" id="apellido_jug" class="form-control" placeholder="Ingrese el apellido">
        </div>
        <div class="col-md-6">
          <br>
          <label for="nombre_jug">NOMBRE:</label>
          <input type="text" name="nombre_jug" id="nombre_jug" class="form-control" placeholder="Ingrese el nombre">
        </div>
      </div>

      <br>
      <label for="estatura_jug">ESTATURA (m):</label>
      <input type="number" name="estatura_jug" id="estatura_jug" class="form-control" step="any" placeholder="Ingrese la estatura">
      <br>

      <label for="salario_jug">SALARIO:</label>
      <input type="number" name="salario_jug" id="salario_jug" class="form-control" step="0.01" placeholder="Ingrese el salario">
      <br>

      <label for="estado_jug">ESTADO:</label>
      <select name="estado_jug" id="estado_jug" class="form-control">
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
      </select>
      <br>

      <label for="id_pos">POSICIÓN:</label>
      <select name="fk_id_pos" id="fk_id_pos" class="form-control">
        <?php if ($posiciones): ?>
          <?php foreach ($posiciones as $posicion): ?>
            <option value="<?php echo $posicion->id_pos; ?>"><?php echo $posicion->nombre_pos; ?></option>
          <?php endforeach; ?>
        <?php else: ?>
          <option value="">No hay posiciones disponibles</option>
        <?php endif; ?>
      </select>
      <br>

      <label for="id_equi">EQUIPO:</label>
      <select name="fk_id_equi" id="fk_id_equi" class="form-control">
        <?php if ($equipos): ?>
          <?php foreach ($equipos as $equipo): ?>
            <option value="<?php echo $equipo->id_equi; ?>"><?php echo $equipo->nombre_equi; ?></option>
          <?php endforeach; ?>
        <?php else: ?>
          <option value="">No hay equipos disponibles</option>
        <?php endif; ?>
      </select>
      <br>
    </div>

    <div class="col-md-6">
      <br>
      <img src="https://static.vecteezy.com/system/resources/previews/010/135/398/original/cartoon-football-soccer-player-man-in-action-png.png" style="width: 800px;"alt="">
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save"></i> GUARDAR</button>
          <a href="<?php echo site_url('jugadores/index');?>" class="btn btn-danger"> <i class="fa fa-ban"></i> CANCELAR</a>
        </div>
      </div>
      <br>
    </div>
  </div>
</form>
<br><br>

<script type="text/javascript">
    $("#formulario_jugador").validate({
        rules: {
            "apellido_jug": {
                required: true,
                minlength: 2,
                maxlength: 500
            },
            "nombre_jug": {
                required: true,
                minlength: 2,
                maxlength: 500
            },
            "estatura_jug": {
                required: true,
                number: true,
                min: 0
            },
            "salario_jug": {
                required: true,
                number: true,
                min: 0
            },
            "estado_jug": {
                required: true
            },
            "id_pos": {
                required: true
            },
            "id_equi": {
                required: true
            }
        },
        messages: {
            "apellido_jug": {
                required: "Por favor, ingrese el apellido del jugador",
                minlength: "El apellido debe tener al menos 2 caracteres",
                maxlength: "El apellido no puede tener más de 500 caracteres"
            },
            "nombre_jug": {
                required: "Por favor, ingrese el nombre del jugador",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 500 caracteres"
            },
            "estatura_jug": {
                required: "Por favor, ingrese la estatura del jugador",
                number: "Ingrese un valor numérico válido",
                min: "La estatura no puede ser negativa"
            },
            "salario_jug": {
                required: "Por favor, ingrese el salario del jugador",
                number: "Ingrese un valor numérico válido",
                min: "El salario no puede ser negativo"
            },
            "estado_jug": {
                required: "Por favor, seleccione el estado del jugador"
            },
            "id_pos": {
                required: "Por favor, seleccione la posición del jugador"
            },
            "id_equi": {
                required: "Por favor, seleccione el equipo del jugador"
            }
        },
        errorClass: "text-danger"
    });
</script>
