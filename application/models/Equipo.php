<?php
class Equipo extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }

  // Insertar nuevos equipos
  function insertar($datos){
    $respuesta=$this->db->insert("equipo",$datos);
    return $respuesta;
  }

  // Consultar todos los equipos
  function consultarTodos(){
    $equipos=$this->db->get("equipo");
    if ($equipos->num_rows()>0){
      return $equipos->result();
    } else {
      return false;
    }
  }

  // Eliminar equipo por id
  function eliminar($id){
    $this->db->where("id_equi",$id);
    return $this->db->delete("equipo");
  }

  // Consultar un único equipo por id
  function obtenerPorId($id){
    $this->db->where("id_equi",$id);
    $equipo=$this->db->get("equipo");
    if ($equipo->num_rows()>0) {
      return $equipo->row();
    } else {
      return false;
    }
  }

  // Función para actualizar equipos
  function actualizar($id,$datos){
    $this->db->where("id_equi",$id);
    return $this->db
                ->update("equipo",$datos);
  }

} // Fin de la clase
?>
