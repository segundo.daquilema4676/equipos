<?php
class Jugador extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Insert new jugador
    function insertar($datos)
    {
        return $this->db->insert("jugador", $datos);
    }

    // Data query
    function consultarTodos()
    {
        $jugadores = $this->db->get("jugador");
        if ($jugadores->num_rows() > 0) {
            return $jugadores->result();
        } else {
            return false;
        }
    }

    // Delete jugador by id
    function eliminar($id)
    {
        $this->db->where("id_jug", $id);
        return $this->db->delete("jugador");
    }

    // Query a single jugador
    function obtenerPorId($id)
    {
        $this->db->where("id_jug", $id);
        $jugador = $this->db->get("jugador");
        if ($jugador->num_rows() > 0) {
            return $jugador->row();
        } else {
            return false;
        }
    }

    // Function to update jugador
    function actualizar($id, $datos)
    {
        $this->db->where("id_jug", $id);
        return $this->db->update("jugador", $datos);
    }
} // End of the class
?>
