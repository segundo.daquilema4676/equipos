<?php
class Posicion extends CI_MODEL
{
    function __construct()
    {
        parent::__construct();
    }

    // Insertar nuevas posiciones
    function insertar($datos)
    {
        $respuesta = $this->db->insert("posicion", $datos);
        return $respuesta;
    }

    // Consultar todas las posiciones
    function consultarTodos()
    {
        $posiciones = $this->db->get("posicion");
        if ($posiciones->num_rows() > 0) {
            return $posiciones->result();
        } else {
            return false;
        }
    }

    // Eliminar posicion por id
    function eliminar($id)
    {
        $this->db->where("id_pos", $id);
        return $this->db->delete("posicion");
    }

    // Consultar una única posicion por id
    function obtenerPorId($id)
    {
        $this->db->where("id_pos", $id);
        $posicion = $this->db->get("posicion");
        if ($posicion->num_rows() > 0) {
            return $posicion->row();
        } else {
            return false;
        }
    }

    // Función para actualizar posiciones
    function actualizar($id, $datos)
    {
        $this->db->where("id_pos", $id);
        return $this->db->update("posicion", $datos);
    }
} // Fin de la clase
?>
