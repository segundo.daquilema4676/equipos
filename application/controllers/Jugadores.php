<?php
class Jugadores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Jugador");
        $this->load->model("Equipo");
        $this->load->model("Posicion");
    }

    public function index()
    {
        $data["listadoPosiciones"] = $this->Posicion->consultarTodos();
        $data["listadoEquipos"] = $this->Equipo->consultarTodos();
        $data["listadoJugadores"] = $this->Jugador->consultarTodos();

        $this->load->view("header");
        $this->load->view("jugadores/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idJugador)
    {
        $this->Jugador->eliminar($idJugador);
        $this->session->set_flashdata("confirmacion", "Jugador eliminado correctamente");
        redirect("jugadores/index");
    }

    public function nuevo()
    {
        $data["posiciones"] = $this->Posicion->consultarTodos();
        $data["equipos"] = $this->Equipo->consultarTodos();

        $this->load->view("header");
        $this->load->view("jugadores/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarJugador()
    {
        $datosNuevoJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi")
        );
        $this->Jugador->insertar($datosNuevoJugador);
        $this->session->set_flashdata("confirmacion", "Jugador guardado exitosamente");
        redirect('jugadores/index');
    }

    public function editar($idJugador)
    {
        $data["posiciones"] = $this->Posicion->consultarTodos();
        $data["equipos"] = $this->Equipo->consultarTodos();
        $data["jugadorEditar"] = $this->Jugador->obtenerPorId($idJugador);

        $this->load->view("header");
        $this->load->view("jugadores/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarJugador()
    {
        $idJugador = $this->input->post("id_jug");

        $datosJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi")
        );

        $this->Jugador->actualizar($idJugador, $datosJugador);
        $this->session->set_flashdata("confirmacion", "Jugador actualizado exitosamente");
        redirect('jugadores/index');
    }
}
?>
