<?php
class Equipos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Equipo");

        // Disable PHP errors and warnings
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoEquipos"] = $this->Equipo->consultarTodos();
        $this->load->view("header");
        $this->load->view("equipos/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idEquipo)
    {
        $this->Equipo->eliminar($idEquipo);
        $this->session->set_flashdata("confirmacion", "Equipo eliminado correctamente");

        redirect("equipos/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("equipos/nuevo");
        $this->load->view("footer");
    }

    public function guardarEquipo()
    {
        $datosNuevoEquipo = array(
            "nombre_equi" => $this->input->post("nombre_equi"),
            "siglas_equi" => $this->input->post("siglas_equi"),
            "fundacion_equi" => $this->input->post("fundacion_equi"),
            "region_equi" => $this->input->post("region_equi"),
            "numero_titulos_equi" => $this->input->post("numero_titulos_equi")
        );

        $this->Equipo->insertar($datosNuevoEquipo);
        $this->session->set_flashdata("confirmacion", "Equipo guardado exitosamente");
        redirect('equipos/index');
    }

    public function editar($idEquipo)
    {
        $data["equipoEditar"] = $this->Equipo->obtenerPorId($idEquipo);
        $this->load->view("header");
        $this->load->view("equipos/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarEquipo()
    {
        $idEquipo = $this->input->post("id_equi");

        $datosEquipo = array(
            "nombre_equi" => $this->input->post("nombre_equi"),
            "siglas_equi" => $this->input->post("siglas_equi"),
            "fundacion_equi" => $this->input->post("fundacion_equi"),
            "region_equi" => $this->input->post("region_equi"),
            "numero_titulos_equi" => $this->input->post("numero_titulos_equi")
        );

        $this->Equipo->actualizar($idEquipo, $datosEquipo);
        $this->session->set_flashdata("confirmacion", "Equipo actualizado exitosamente");
        redirect('equipos/index');
    }

}
?>
