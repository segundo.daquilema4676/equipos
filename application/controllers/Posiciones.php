<?php
class Posiciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Posicion");

        // Disable PHP errors and warnings
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoPosiciones"] = $this->Posicion->consultarTodos();
        $this->load->view("header");
        $this->load->view("posiciones/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id_pos)
    {
        $this->Posicion->eliminar($id_pos);
        $this->session->set_flashdata("confirmacion", "Posición eliminada correctamente");

        redirect("posiciones/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("posiciones/nuevo");
        $this->load->view("footer");
    }

    public function guardarPosicion()
    {
        $datosNuevaPosicion = array(
            "nombre_pos" => $this->input->post("nombre_pos"),
            "descripcion_pos" => $this->input->post("descripcion_pos")
        );

        $this->Posicion->insertar($datosNuevaPosicion);
        $this->session->set_flashdata("confirmacion", "Posición guardada exitosamente");
        redirect('posiciones/index');
    }

    public function editar($id_pos)
    {
        $data["posicionEditar"] = $this->Posicion->obtenerPorId($id_pos);
        $this->load->view("header");
        $this->load->view("posiciones/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarPosicion()
    {
        $id_pos = $this->input->post("id_pos");

        $datosPosicion = array(
            "nombre_pos" => $this->input->post("nombre_pos"),
            "descripcion_pos" => $this->input->post("descripcion_pos")
        );

        $this->Posicion->actualizar($id_pos, $datosPosicion);
        $this->session->set_flashdata("confirmacion", "Posición actualizada exitosamente");
        redirect('posiciones/index');
    }
}
?>
